import { Component, OnInit } from '@angular/core';
import { PlaceholderService } from './placeholder.service';
import { ModelService } from './model.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  frmNumber:number = 1
  choices:Object[] = [{cat: 'people'},{cat: 'planets'},{cat: 'vehicles'},{cat: 'species'},{cat: 'starships'}]
  whichCategory:string
  public form: Object;

  constructor(private placeholderService:PlaceholderService){
    this.form = new ModelService();
  }

  handleClick(){
    this.placeholderService.getParamData(this.frmNumber, this.whichCategory)
    .subscribe((result)=>{this.form=result})
  }
}
