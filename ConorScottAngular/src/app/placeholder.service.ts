import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {

  apiURL:string = 'https://swapi.co/api/'

  constructor(private http:HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {

      console.error(error);
      alert("Not found")

      return throwError('Something bad happened; please try again later');
    }
  }

  //declare methods of the service
  getParamData(number, category){
    return this.http.get(`${this.apiURL}${category}/${number}`)
    .pipe(
      catchError(this.handleError<Object[]>('getParamData',[])
      )
    )
  }
}
